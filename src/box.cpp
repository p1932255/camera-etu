#include "box.h"

#include <limits>
#include <algorithm>
#include <iostream>

float Box::hit(const Point& origin, const Vector& direction) {

  //implement this method
  Vector invd = Vector(1./direction.x, 1./direction.y, 1./direction.z);
  Vector t1 = (min-origin)*invd;
  Vector t2 = (max-origin)*invd;
  Vector tmin = Vector(std::min(t1.x,t2.x),std::min(t1.y,t2.y),std::min(t1.z,t2.z));
  Vector tmax = Vector(std::max(t1.x,t2.x),std::max(t1.y,t2.y),std::max(t1.z,t2.z));
  
  float min=0., max=std::numeric_limits<float>::infinity();
  if (tmin.x>min and tmin.x>0) min = tmin.x;
  if (tmax.x<max) max = tmax.x;
  if (tmin.y>min and tmin.y>0) min = tmin.y;
  if (tmax.y<max) max = tmax.y;
  if (tmin.z>min and tmin.z>0) min = tmin.z;
  if (tmax.z<max) max = tmax.z;
  if (min<=0 and max>0) min=max;
  if (min>max) 
    min = std::numeric_limits<float>::infinity();
  return min;
}
