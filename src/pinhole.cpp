#include "pinhole.h"
#include "mat.h"

#include <iostream>

#include <cmath>

Point Camera::pixel(int i, int j) {
  
  //implement this method
  Vector unit_dir=normalize(direction);
  Vector dirx = -normalize(cross(up,direction))/resolution[0];
  Vector diry = -normalize(cross(direction,dirx))/resolution[1];
  
  float dist=0.2;
  
  float w = 2*std::tan(angle*M_PI/360);
  float h = 2*std::tan(angle*M_PI/360);
  
  int posx = i-resolution[0]/2.;
  float x = posx+0.5;
  
  int posy = j-resolution[1]/2.;
  float y = posy+0.5;
  
  Vector hole_point = unit_dir+x*dirx*w+y*diry*h;
  return hole + dist*hole_point;
}
