#include "triangle.h"

#include "mat.h"

#include <limits>

float triangle_hit(const Point& origin, const Vector& direction, const Point* pts) {
  float det0 = dot(cross(origin-pts[0], origin-pts[1]),direction);
  float det1 = dot(cross(origin-pts[1], origin-pts[2]),direction);
  if(det0*det1<0) return std::numeric_limits<float>::infinity();
  float det2 = dot(cross(origin-pts[2], origin-pts[0]),direction);
  if(det0*det2<0) return std::numeric_limits<float>::infinity();
  
  Vector n = cross(pts[1]-pts[0], pts[2]-pts[0]);
  
  return dot((pts[0]-origin),n)/dot(direction,n) ;
}
